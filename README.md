# Teravoz - Full Stack Challenge

## Tarefas

**1. Orgulho de**

Este é um ponto estranho, pois sempre que olho para traz vejo que algo deveria ser refatorado...

Enfim, segue projeto de conexão com o Google Cloud para gestão de armazenamento em buckets distintos pelo tempo de upload. Em termos práticos após o upload a imagem permanece em um ambiente local por 15 dias, após 15 dias ela é enviada ao bucket com uma resposta mais lenta e após 90 dias ele é transferido para o bucket de archive.

Isto nos trouxe redução de custo no ambiente de prod, em relação ao tamanho do armazenamento...se referindo ao custo de um SSD, e escalabilidade de archive com um custo bem baixo.

O principal desafio foi a utilização de JWT para comunicação.

Confira: [BitBucket](https://bitbucket.org/rsbegue/gcloud/src/master/)

**2. Vergonha de**

Ah com certeza fazer web scrapy entre sistemas...sempre me soa como gambiarra.
Hoje porem com um jeito mais bonito de fazer com jSoap e Beautiful Soup.

```
<cffunction name="ProcessaHtml">
	<cfargument name="p_html"  required="true" />

	<cfset reLimpaTag = '</?\w+((\s+\w+(\s*=\s*(?:.*?|".*?"|''.*?''|[^''">\s]+))?)+\s*|\s*)/?>'>

	<cfset conteudo_tabela = ReMatchNoCase('<table[^<]*>(.*?)</table>', arguments.p_html)>

	<cfif arrayLen(conteudo_tabela) eq 0>
		<cfset erro_validacao = 1>
	</cfif>

	<cfloop from="1" to="#arrayLen(conteudo_tabela)#" index="index_tabela">

		<cfset conteudo_tabela[index_tabela] = util.htmlDecode(conteudo_tabela[index_tabela]) />
		<cfset conteudo_linha = ReMatchNoCase('<tr[^<]*>(.*?)</tr>', conteudo_tabela[index_tabela])>

		<cfif arrayLen(conteudo_linha) eq 0>
			<cfset erro_validacao = 1><cfset erro_msg = 'O arquivo n�o possui uma estrutura v�lida'>
		</cfif>

		<cfloop from="1" to="#arrayLen(conteudo_linha)#" index="index_linha">

			<cfset idxPasso = 1 />
			<cfset conteudo_coluna = ReMatchNoCase('<td[^<]*>(.*?)</td>', conteudo_linha[index_linha]) />

			<cfset arrItemTitulo = reMatchNoCase("<td[^<]*>(.*?)(opera)(.*?)(o)(.*?)</td>(.*?)<td[^<]*>(.*?)(c)(.*?)(digo)(.*?)</td>(.*?)<td[^<]*>(.*?)(descri)(.*?)(o)(.*?)</td>(.*?)<td[^<]*>(.*?)(qtde)(.*?)</td>(.*?)<td[^<]*>(.*?)(pre)(.*?)(o)(.*?)(un)(.*?)</td>(.*?)<td[^<]*>(.*?)(pre)(.*?)(o)(.*?)(tot)(.*?)</td>(.*?)<td[^<]*>(.*?)(desc)(.*?)</td>(.*?)<td[^<]*>(.*?)(hora)(.*?)</td>(.*?)<td[^<]*>(.*?)(pintura)(.*?)</td>", conteudo_linha[index_linha]) />


			<cfif arrayLen(arrItemTitulo) gt 0>
				<cfset idxLinhaTitulo = index_linha />
			</cfif>

			<cfset arrItemLinha = reMatchNoCase("<td[^<]*>(.*?)(troca)(.*?)</td>(.*?)<td[^<]*>(.*?)</td>(.*?)<td[^<]*>(.*?)</td>(.*?)<td[^<]*>([^,.]{0,})([0-9]{1,})([^,.]{0,})</td>(.*?)<td[^<]*>(.*?)([0-9.]{1,}[,]{1}[0-9]{2})(.*?)</td>(.*?)<td[^<]*>(.*?)([0-9.]{1,}[,]{1}[0-9]{2})(.*?)</td>(.*?)<td[^<]*>(.*?)([0-9]{1,3}[,]{1}[0-9]{2})(.*?)</td>(.*?)", conteudo_linha[index_linha]) />


			<cfif idxLinhaTitulo gt 0 and idxLinhaTitulo lt index_linha>
				<cfset existeItemDNC = (arrayLen(ReMatchNoCase('<td[^<]*>(.*?)(DNC)(.*?)</td>', conteudo_linha[index_linha])) gt 0) />
			<cfelse>
				<cfset existeItemDNC = false />
			</cfif>
			<cfif not existeItemDNC>
				<cfset booLinhaItem = (arrayLen(arrItemLinha) gt 0) />
				<cfloop from="1" to="#arrayLen(conteudo_coluna)#" index="index_coluna">
					<cfset conteudo_campo = trim(ReReplaceNoCase(conteudo_coluna[index_coluna],reLimpaTag,'','all'))>
					<cfset conteudo_campo = ReReplaceNoCase(conteudo_campo,"(\r\n)",'','all')>
					<cfset conteudo_campo = replace(conteudo_campo,chr(160),'','all')>
					<cfset conteudo_campo = reReplace(conteudo_campo, '[ ]{2,}', ' ', 'all') />


					<cfif index_linha GT idxLinhaTitulo and booLinhaItem>
						<cfif aux_linha_itens IS NOT index_linha >
							<cfset structItens.pecaDNC = '' />
							<cfset structItens = structNew() />
							<cfset structItens.linha = index_linha />
						</cfif>

						<cfset _conteudo_campo = reReplaceNoCase(conteudo_campo,"[^\w\d\s.,]","","all") />
							<cfswitch expression="#idxPasso#">
								<cfcase value="1">
									<cfif arrayLen(ReMatchNoCase('(.*?)(TROCA)(.*?)', _conteudo_campo)) gt 0>
										<cfset structItens.pecaOperacao = conteudo_campo />
										<cfset idxPasso = 2 />
									</cfif>
								</cfcase>
								<cfcase value="2">
									<cfif len(trim(conteudo_campo)) gt 0>
										<cfset structItens.pecaCodigo = conteudo_campo />
										<cfset idxPasso = 3 />
									</cfif>
								</cfcase>
								<cfcase value="3">
									<cfif len(conteudo_campo) gt 2>
										<cfset structItens.pecaDescricao = conteudo_campo />
										<cfset idxPasso = 4 />
									</cfif>
								</cfcase>
								<cfcase value="4">
									<cfif arrayLen(ReMatchNoCase('^([0-9]+[.]*)$', _conteudo_campo)) gt 0>
										<cfset structItens.pecaQuantidade = conteudo_campo>
										<cfset idxPasso = 5 />
									</cfif>
								</cfcase>
								<cfcase value="5">
									<cfif arrayLen(ReMatchNoCase('([0-9]+[.]*)([,]?)([0-9]{2})', _conteudo_campo)) gt 0>
										<cfset structItens.pecaPrecoUnitario = conteudo_campo />
										<cfset idxPasso = 6 />
									</cfif>
								</cfcase>
								<cfcase value="6">
									<cfif arrayLen(ReMatchNoCase('([0-9]+[.]*)(,)([0-9]{2})', _conteudo_campo)) gt 0>
										<cfset idxPasso = 7 />
									</cfif>
								</cfcase>
								<cfcase value="7">
									<cfif arrayLen(ReMatchNoCase('([0-9]{1,3}[.]*)(,)([0-9]{2})', conteudo_coluna[index_coluna])) gt 0>
										<cfset structItens.pecaDesconto = conteudo_campo />
										<cfset structItens.pecaDNC = ''>
										<cfset arrayAppend(arrayItens, structItens) />
										<cfset idxPasso = 8 />
									</cfif>
								</cfcase>
							</cfswitch>
						<cfset aux_linha_itens = index_linha>
					<cfelse>
						<cfset dados[index_tabela][index_linha][index_coluna] = conteudo_campo />	
					</cfif>

				</cfloop>
			</cfif>
		</cfloop>
	</cfloop>	
</cffunction>
```

**3. Teravoz API**
#### Requirements
* NodeJS
* Docker
* Postman

```
Build a docker image:
$ docker build -t rsbegue/teravoz-challenge .

Run image:
$ docker run -p 3000:5000 -d rsbegue/teravoz-challenge

Verifique se o serviço está ativo:
$ curl -i localhost:3000
```
Import Postman file to calls requests:
[Teravoz-Challenge.postman_collection.json](https://bitbucket.org/rsbegue/teravoz-challenge/src/master/Teravoz-Challenge.postman_collection.json)

#### Main Services
```
GET - /actions: return json list
GET - /clientes: return json list
GET - /eventos: return json list
GET - /startCalls: start fake calls
GET - /stopCalls: stop fake calls
POST - /webhook: receive post parameters to check and delegate
```

_bonus 1_: Projeto rodando em docker ;)

_bonus 2_: Projeto sem o painel de chamadas em React :(

**4. Feedback**

Creio que a ideia do desafio foi excelente, pois só tinha feito um "Hello World!" em NodeJS. Ele me fez me aprofundar mais na linguagem, bibliotecas, alguns padrões e ter a certeza que com o JS existem milhões maneiras de se fazer a mesma coisa. Senti falta um modo de debug mais visual, mas creio que isto deve ter nos github da vida.

Pelo tempo decidi enviar somente a parte em NodeJS, pois definitivamente a curva de aprendizado para React é um pouco longa e tem alguns padrões diferentes do que vi ultimamente...e este foi meu ponto negativo no desafio.