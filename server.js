/**
 * Plugins inits
 */
const express    = require('express');
const bodyParser = require('body-parser');
const low 		 = require('lowdb');
const FileSync 	 = require('lowdb/adapters/FileSync');
const adapter 	 = new FileSync('db.json');
const db  		 = low(adapter);
const request 	 = require('request');

/**
 * ENV inits
 */
const app = express();
const port = process.env.PORT || 5000;
const host = process.env.HOST || "localhost";
const apiTeraVozEndPoint = (host == 'localhost') ? "http://"+host+":"+port : "https://api.teravoz.com.br"; 

/**
 * LowDb init default
 */
db.defaults({
	clientes: [],
    eventos:  [],
    actions:  []
}).write();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

let calls;

/**
 * Webhook to queue treatment by the received number
 */
app.post('/webhook', (req, res) => {

	if( req.body.type == undefined )
		return res.json({ status: false, message: 'Parametro "type" não informado. Por favor verifique a documentação.' });

	db
	 .get('eventos')
	 .push(req.body)
	 .write();

	if( req.body.type != "call.standby" )
		return res.json({ status: false, message: 'Evento: ' + req.body.type + ', não implementado.' });

	let ramal   = "";
	let message = "";
	let hasClient = db.get('clientes').filter({phone_number: req.body.their_number}).value();

	if( hasClient.length == 0 ){
		db
		 .get('clientes')
		 .push({phone_number: req.body.their_number})
		 .write();

		ramal   = "*2900";
		message = 'Chamada encaminhada para fila de primeiro atendimento'; 
	}else{
		ramal   = "*2901";
		message = 'Chamada encaminhada para fila de clientes'; 
	}

	request
	 .post({
		url:  apiTeraVozEndPoint + "/actions",
		json: {
		  "type": 		 "delegate",
		  "call_id": 	 req.body.call_id,
		  "destination": ramal
		}
	 })
	 .on('response', (response) => {
	 	if(response.statusCode == "200"){
            db
             .get('actions')
             .push({
                "type": 		 "delegate",
                "call_id": 	 req.body.call_id,
                "destination": ramal
             }).write();
	 		return res.json({ status: true, ramal: ramal, message: message });	
	 	}else{
	 		return res.json({ status: false, ramal: ramal, message: "Ocorreu um erro ao redirecionar a chamada." });	
	 	}
	 })
	 .on('error', (err) => {
		return res.json({ status: false, error: err });
	 });
	
});

/**
 * Start fake calls
 */
app.get('/startCalls', (req, res) => {

	let type = ["call.new","call.standby","call.waiting","actor.entered","call.ongoing","actor.left","call.finished"];

	calls = setInterval(() => {
		request(
            {
                uri :    apiTeraVozEndPoint + "/webhook",
                method : "POST",
                json :   {
                    type: 		type[Math.floor(Math.random() * type.length)],
                    call_id: 	Math.floor(Math.random() * 9999999999)+"."+Math.floor(Math.random() * 99999),
                    code: 		"123456",
                    direction: 	"inbound",
                    our_number: "0800000000",
                    their_number: "1199191000" + Math.floor(Math.random() * 10),
                    timestamp: 	new Date().toISOString()
                }
            },
            function(error, response, body){
                console.log(body);
            }
        );
    }, 1500);
	
	res.send('Chamadas em andamento!');
});

/**
 * Stop fake calls
 */
app.get('/stopCalls', (req, res) => {
    clearInterval(calls);

    res.send('Chamadas encerradas!');
});

/**
 * Fake TeraVoz endpoint API
 */
app.post('/actions', (req, res) => {
	console.log(req.body);
	res.json({status: "ok"});
});

/**
 * Return lowdb
 */
app.get('/clientes', (req, res) => {
    res.send(
        db.get('clientes').value()
    );
});

app.get('/eventos', (req, res) => {
    res.send(
        db.get('eventos').value()
    );
});

app.get('/actions', (req, res) => {
    res.send(
        db.get('actions').value()
    );
});

app.listen(port, () => console.log(`Listening on port ${port}`));